import Demo.EmployeeRequest;
import Demo.EmployeeResponse;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class Employee {

    @Test
    public void getEmployee() {
        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com")
                .basePath("/api")
                .log()
                .all()
                .header("Content-Tyoe", "application/json")
                .header("Accept", "application/json")
                .get("/v1/employees");

       // response.getBody().prettyPrint();
        System.out.println("response status code : " + response.getStatusCode());
        Assert.assertEquals(200, response.getStatusCode());
        Assert.assertThat("Taking a long time", response.getTime(),
            Matchers.lessThan(4000L)); //L buat pindah ke tipe data long

        Assert.assertEquals("success", response.path("status"));
        Assert.assertEquals("Ashton Cox", response.path("data[2].employee_name"));

        EmployeeResponse employeeResponse = response.as(EmployeeResponse.class);//serializer
        System.out.println("status : " + employeeResponse.getStatus());
        System.out.println(employeeResponse.getData().get(0).getEmployeeName());

    }

    @Test
    public void createEmployee(){
        EmployeeRequest employeeRequest = new EmployeeRequest();//deserilizer
        employeeRequest.setName("Riekko");
        employeeRequest.setAge("23");
        employeeRequest.setSalary("12313131");

        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com")
                .basePath("/api")
                .log()
                .all()
                .header("Content-Tyoe", "application/json")
                .header("Accept", "application/json")
                .body(employeeRequest)
                .post("/v1/create");

        response.getBody().prettyPrint();

    }
}
